# Simple Home Automation

This is my implementation of Task B -a javascript application simulating home automation.

Before reviewing the code please read the [Technical considerations](#technical_considerations) section in this document. 

This application has been tested with Chrome (73) and Firefox (67) and Windows 10 as Operating system.


#### Quickstart

- git clone https://gitlab.com/kmersman/simplehomeautomation

- copy content of ./simplehomeautomation/WebContent (with subfolders) to WebServer Content directory

- Access via Context of your WebServer, e.g. http://localhost

- Click on any room in the displayer picture
 
 
 
 To see a demonstration of the extension mechanism:
 
 - Uncomment lines 21-23 in index.html
 
 - Reload in your browser
 
 - Click on the kitchen, a slider contronl will be shown instead of the +/- & Textfiled control for room-temperature
 
 

#### Using the application

Deploy on any Web Server or Java Application Server/Web Container that can serve static content. 
The application itself is in the /WebContentFolder

In case a Java Application Server/ Web Container is used, no server side functionality is used (as requested in the task definition). 

The user can invoke the application simply by accessing the context-root, e.g. localhost:8080/simplehomeautomation.

The UI has two parts, the image section (on the left with the svg- image and the control panel to display the controls for the selected room.) 

By clicking on a room the controls for that room are shown in the control panel and then can be used to alter the settings.
(Control- pane is hidden and becomes only visible after a room is selected.)

![alt text](./doc/application.png "Application Screenshot")

Unless hosted on a Web Server that allows put-requests changes are not persisted. Please see technical explanations below.

### Application structure


The "home" that is controlled is represented via a configuration file and a SVG image

- configuration-file: A json file for the automation configuration data (data/samplehouse.json)
- SVG image: Visual representation of the controlled home (data/samplehouse.svg)

- The implementation of the application logic is included in js/simplehouseautomation.js
- Extensions are to be implemented in a dedicated JS-File. The Sample extension is in js/addon.js.


To add a different home, add a new configuration-file and a new SVG Image somewhere (e.g. WebContent/data2).
Change index.html to point to the new files instead of the existing ones (see below).

    $(document).ready(docReady("data/samplehouse.json","data/samplehouse.svg"));


#### The configuration file 

The json- configuration file needs to contain an array called *rooms*. 
 
Each Object in this array represents a single room.  

Each room needs to have the following Attributes

- id: Uniquely identifies each room and links it with it's visual representation in the SVG file.

- devices: Array with device objects (see below).


Each *room* object has an array of *devices* representing the controls for that specific room.

    {
     "house" : {
     "name" : "My flat",
     "label": "This is home control for my flat",
    "rooms": [
    {
     "id": "room1",
     "devices": [
       {
          "id": "room1_light",
          "type": "boolean_control",
          "label": "Deckenbeleuchtung",
          "val_default" :"false",
          "val_on": "1",
          "label_on": "Licht an",
          "val_off": "0",
          "label_off": "Licht aus",
          "status_url": "devices/room1/room1_light/status.json"
          },
        {
            "id": "room1_curtains",
            "type": "boolean_control",
            "label": "Vorhaenge",
            "val_default" :"false",
            "val_on": "1",
            "label_on": "Vorhaenge auf",
            "val_off": "0",
            "label_off": "Vorhaenge geschlossen",
            "status_url": "devices/room1/room1_curtains/status.json"
            },
    
       ....
 

Each device object needs to have the following attributes

- id type Attribute: Uniquely identifies the device 

- type Attribute: Defines the type of control to be used for the device

-  status URL: The Endpoint to get/ set device information. 



All other attributes are specific for the defined type. In the base version the following types are included

- boolean_control: For devices with two states on/ off

- discrete_control: For devices with a numerical discrete setting, e.g. temerature controls, fan-speed control, ... 


For each type a renderer is implemented to render the control for the device on the control pane.

The  extension mechanism is based on adding additional types with a specific renderer. See section [Extension Mechanism](extension_mechanism).
 
 

#### The SVG Image

The svg Image is used as a visual representation of the room. 

While basically any svg- image can be used, for the sample-home contained in the application, an SVG Image with two layers has been used. 

One layer containing an JPEG- Image of a floor-plan q.

Above this layer is a second layer containing borderless rectangles with transparent fill color (no-color for filling the rectangles does not work as the click-events are fired). The recantgles are simpley put over the rooms in the JPEG in the lower layer. This way creating clickable, visual representations of the home to be controlled is straight forward.

Important: the Id- Attribute of the recangles must match the Id- for the room in the configuration file.

For the sample InkScape has been used, in the German version the Atrribute to be set is called 'Kennung'.


The general concept is to have SVG- Elements in the Image that have an id-Attribute matching the room id-Attributes found in the configuration file. 

In the example above the room id is *room1*


	 {
     "id": "room1",
     "devices": [
       {


To the SVG- Elements with a matching ID (in case it exists), an onClick handler is added. During the program execution when the corresponding event is fired, the control pane for the corresponding room is displayed.

See below the sample-snippet from the svg- file representing the rectangle for room1


    <rect style="...." id="room1" width="58.173225" height="73.296303" x="168.32623" y="96.42762" .... ry="0"></rect>


 
<sup>1 </sup> Floorplan 'borrowed' from : https://www.roetzer-ziegelhaus.de/haeuser/stadtvilla/


### Technical considerations <a name="technical_considerations"></a>


While this is my first JavaScript application it resembles Java-coding style and surely lacks the typical java-script functional programming style. 

Apart from that I'd like to highlight some points that I am aware of that can need improvement - yet due to unfamiliarity with JavaScript and as I am running out of time I did not manage to fix these now. 

- This aplication does not use explicit classes to hold the model during runtime. 
Values are set/read from the status_url of the devices  and partly from  the widgets in the ui (e.g. the temperature in the input widget of the discrete_control).
To further improve this sample application introducing dedicated objects/ structures for the model and implementing the data- binding would be necessary.

- The **render_discrete_control** has a design flaw.
While the rendering of the +/- buttons is delegated to **render_discrete_controlButton** function, adding the behavior to the button is done in the  function itself.
With some modifications passing the whole function block shown below as parameter could have been possible - yet, the way it is now this would have been an odd way of doing things.


    $("#"+device.id+"_down").button({
		label : "-"
	    }).click(function(event) {

		current= 	parseInt($("#"+device.id+"_text").val());
		if(current > device.val_min)
		  $("#"+device.id+"_text").val(current-1);
	
	});


- Rendering  all controls in the same div-table has drawbacks as the column width of each distinct colum is defined by the maximum wifht of all controsls for the room.
Rendering each Conrol in a distinct table could improve this behavior.



####  The control pane <a name="control_pane"></a>

The control pane is rendered as a div table, where each table row is used to render one control. 

Each renderer has to render a single table-row of this div- Table.

While technically there are no restrictions, the Label (device.label) should be rendered in the first column. 

To provide a better understanding please find the dom of a rendered control below. 

![alt text](./doc/control_pane_dom.png "Dom of the Control Pane")





#### Getting and Setting control- data  <a name="device_data"></a>


The current "sensor-data"  are requested via HTTP-Get call and written as HTTP-Put. (For implementation details please see  functions getDeviceStatus and
setDeviceStatus.)

It is assumed that each control has a dedicated endpoint to be read/set by getting/putting a json file. So the structure of the sensors in the house is mimicked under the devices directory in the sample delivered with this application. Generally each control defines it's own endpoint via the status_url attribute in the configuration file, e.g 


    ...
    "status_url": "devices/room3/room3_light/status.json"
    ...

While each renderer can use it's own format for this, in the application and the sample extension the following formats are used

- For the controls using the boolean_control
    
    {
      "status": "true"
    }

- For the controls using the discrete_control or the advanced_control in teh sample extension
    
    {
     "status": "19"
    }

Unless a WebServer is used, that allows resource creation via put, the setDeviceStatus calls will result in an exception, logged to the javascript console.






#### The extension mechanism <a name="extension_mechanism"></a> 


The extension mechanism allows to write new renderer-implementations for controls. These new renderer-implementations can then be applied to controls defined in the config file.

Technically the extension mechanism is based on the selection of the correct renderer function based on the device.type attribute.

The correct function name is inferred and invoked via the JS eval function:


    function addDeviceToControlPane(device,divTableRow){
	
	 	var  renderrerFunctionNameforDevicType="render_"+device.type+"(device,divTableRow)";
		eval(renderrerFunctionNameforDevicType);

    }
    
 Controls that shall use this extension need to set the type attribute to the new renderer type, e.g:
 
 - The renderer is called **advanced_control**.   
 - A function **render_advanced_control(device,divTableRow)** needs to be provided 
 - In the config file, controls that shall use this extension need to set the type attribute to **advanced_control**
 
     ...
     "id": "room1_temperature",
                "type": "advanced_control",
                "label": "Raumtemperatur",
     ...


Extensions should also use the getDeviceStatus and setDeviceStatus functions to request/ set the control values to the endpoint- though any other mechanism can be implemented in the extended renderer if necessary.

    
##### Sample extension   
The sample extension is implemented in the file js/addon.js. 
For demonstration purposes I utilized some random js-widget library- gijo.js- to implement the sample extension.

To enable the extension uncomment the following lines at the beginning of index.html

    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/addon.js"></script>

The renderer is implemented in js/addon.js

The control_type is called **advanced_control**, so the function implemented is 

    render_advanced_control(device,divTableRow){ ...

Room 3 - K�che  is setup to use the advanced_control.

All other discrete_controls can be updated by changing the type attribute from discrete_control to advanced_control.

The extension improves the application by providing a slider for the discrete control instead of +/- Buttons and a Text field. See screenshot below. 

![alt text](./doc/application_extended.png "Application Screenshot with extension")



