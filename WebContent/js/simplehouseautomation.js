/**
 *simplehouseautomation.js - Main Java Script File for the implementation of
 * the Simple Home Automation project. Processing should be triggered by
 * invoking the docReady() function in this file from the HTML-page that is
 * including this file, e.g. by 
 * <script> ... $(document).ready(docReady); </script>
 **/ 



// Holds data of the 'controlled' Home
var HOME;


/**
 * Function to initialize the application. 
 * Usually to be invoked by the document.ready function
 * 
 */
function docReady(config_file, svg_file){
	loadHome(config_file);
	loadSvg(svg_file);
}



/**
 * Load config file defining the home under control
 * @param config_file
 * @returns
 */
function loadHome(config_file){
	  $.get(config_file, {},function(data){
		  HOME= data["house"];
	  }, 'json');
}

/**
 * Load Svg-File representing the home under control
 * @param svg_file
 * @returns
 */
function loadSvg(svg_file){
    var s = Snap("#homesvg");
    var g = s.group();
    var svgElem = Snap.load(svg_file, function ( loadedFragment ) {
                                                    g.append( loadedFragment );
                                                    addRoomEventListener(g);
                                            } );
}


/**
 * Iterate all rooms in the HOME-configuration and add onclick- Handler
 * @param g
 * @returns
 */
function addRoomEventListener(g){
	
	 $.each(HOME.rooms, function(k, v) {

		 	console.log("Trying to get " + v.id);
		 	var xx = $("#"+v.id );
		 	var yy = xx[0];
		 		yy.onclick=
					function(e){
						addControlsForRoom( event.target.attributes.id.nodeValue);
						};
		
  });
	
}



function addControlsForRoom(roomid){
	
	// Clear the control- panel
	$("#controls").empty();
	$("#controls").append("<h2> Selected: "+roomid+ "</h2>");
	
	// Create Table structure and append to #controls element
	var tableDiv= $('<div>').addClass('divTable');		
	var tableBody= $('<div>').addClass('divTableBody');		
	tableDiv.append(tableBody);
	$("#controls").append(tableDiv);

	/* Iterate rooms in HOME-configuration and add Devices to control pane, 
	 in case the correct room is selected */
	$(HOME.rooms).each(
	 function(k,v){
		 
		if(roomid == v.id){
			addDevicesToControlPane(v.devices, tableBody);
		}
	 }
	)	 
	
	
}

/**
 * Add a single device/ control to the Control Pane by addin a single row to the div-table
 * @param roomJson
 * @param tableBody
 * @returns
 */
function addDevicesToControlPane(roomJson, tableBody){
	
	$(roomJson).each(

			function(key,device){
				
				var divTableRow = $('<div>').addClass('divTableRow');		
				tableBody.append(divTableRow);
				addDeviceToControlPane(device,divTableRow);
 			
			});

}

/**
 * Pick Device Control Rendering function  based on device.type
 * This is the essentialy the key part of the implementation of the extension mechanism
 * @param device
 * @param divTableRow
 * @returns
 */
function addDeviceToControlPane(device,divTableRow){

	var renderrerFunctionNameforDevicType="render_"+device.type+"(device,divTableRow)";
	eval(renderrerFunctionNameforDevicType);
	
}

/**
 * Per default for each control teh device.label is rendered as the first column in the div-table
 * @param device
 * @param divTableRow
 */
function renderDeviceLabel(device, divTableRow){
	
	var divTableCell = $('<div>').addClass('divTableCell');	
	divTableCell.append("<label for='"+device.id+"_label'>"+device.label  +"</label>");
	divTableRow.append(divTableCell);

}


/**
 * Renderer for the controls using the boolean_control type
 * @param device -- Device to render control for
 * @param divTableRow -- Row of the Div-Table to which the control shall be rendered
 */
function render_boolean_control(device,divTableRow){

	var deviceStatusJson =JSON.parse(getDeviceStatus(device));  
	var statOnly;
	
	if( deviceStatusJson){
	 var statOnly = deviceStatusJson["status"];
	}
	else{
		
		statOnly = device.val_default;
		
	}
	
	var checked="";
	if("true" == statOnly){
		
		checked=" checked ";
		
	}
	console.log(device.id + " : "+ statOnly);
	
	renderDeviceLabel(device, divTableRow);

	var divTableCell = $('<div>').addClass('divTableCell');	
	divTableCell.append("<input type='checkbox' name='"+device.id+"' "+checked + "' id='"+device.id+"'>");
	divTableRow.append(divTableCell);

	$("#"+device.id).click(function(event) {
			
		    setDeviceStatus(device, event.target.checked);
			
	});

}

/**
 * Renderer for the controls using the discrete_control type
 * @param device -- Device to render control for
 * @param divTableRow -- Row of the Div-Table to which the control shall be rendered
 */
function render_discrete_control(device,divTableRow){

	var deviceStatusJson =JSON.parse(getDeviceStatus(device));  
	var statOnly;
	
	if( deviceStatusJson){
	 var statOnly = deviceStatusJson["status"];
	}
	else{
		statOnly = device.val_default;
	}
	
	
	// Render device control in div-Table row
	
	renderDeviceLabel(device,divTableRow);
	render_discrete_controlButton(device, divTableRow, "_down");
	render_discrete_controlButton(device, divTableRow, "_up");
	
	// Render input to display requested temperature
	var divTableCell1 = $('<div>').addClass('divTableCell');	
	divTableCell1.append("<input type='text' readonly='true' size='4' value='"+statOnly+"' id='"+device.id+"_text' />" );
	divTableRow.append(divTableCell1);
	
	// Add behavior to control
	
	// Functionality for + Button, e.g. to increase temperature
	$("#"+device.id+"_up").button({
		label : "+"
	}).click(function(event) {

		current= 	parseInt($("#"+device.id+"_text").val());
		if(current < device.val_max)
		  $("#"+device.id+"_text").val(current+1);
		
	});
	
	// Functionality for -Button, e.g. to decreasetemperature
	$("#"+device.id+"_down").button({
		label : "-"
	}).click(function(event) {

		current= 	parseInt($("#"+device.id+"_text").val());
		if(current > device.val_min)
		  $("#"+device.id+"_text").val(current-1);
	
	});
	
}

/**
 * Utility function to render Buttons for the discrete_control control type 
 * 
 **/
function render_discrete_controlButton(device, divTableRow, button_suffix){
	
	var button_id = device.id + button_suffix;
	
	var divTableCell3 = $('<div>').addClass('divTableCell');	
	divTableCell3.append("<button class='ui-button ui-widget ui-corner-all' id='"+button_id+"' />" 	);
	divTableRow.append(divTableCell3);
	
}


/**
 * Get the status of a device
 * @param device - device to read status for
 * @returns
 */
function getDeviceStatus(device){

	var result = null;
      var scriptUrl = device.status_url;
      $.ajax({
         url: scriptUrl,
         type: 'get',
         dataType: 'html',
         async: false,
         success: function(data) {
             result = data;
         } 
      });
      return result;
 
}


/**
 * Set the device status
 * @param device - device to set status for
 * @param state -  status to set
 * @returns
 */
function setDeviceStatus(device, state){
	$.ajax({
	    type: 'PUT',
	    url: device.status_url,
	    contentType: 'application/json',
	    data: JSON.stringify({status : new String(state)})
	}).done(function () {
	    console.log('SUCCESS');
	}).fail(function (msg) {
	    console.log('FAIL');
	}).always(function (msg) {
	});
}

